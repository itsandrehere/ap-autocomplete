import { ModuleWithProviders } from '@angular/core';
export declare class NgxGeoautocompleteModule {
    static forRoot(): ModuleWithProviders;
}
